FROM huggingface/transformers-pytorch-cpu

RUN apt-get update && apt-get install -y \
    python3-dev \
    python3-pip \
    && pip3 install --upgrade pip

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip3 install --upgrade pip
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

CMD ["py setup.py"]

