import nltk
from transformers import AutoTokenizer, AutoModel
import torch
import json
import sys
import codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())


class NLPRecommender:
    def __init__(self, base_model_path='./models/rubert-base-cased', stopwords_path='./models/stopwords.json'):
        with open(stopwords_path, 'r') as file:
            self.stopwords = json.load(file)

        self.tokenizer = AutoTokenizer.from_pretrained(base_model_path + "/tokenizer")
        self.model = AutoModel.from_pretrained(base_model_path + "/model")

        self.samples = []
        self.samples_embeddings = []


    def _preprocess_text(self, text):
        tokenizer = nltk.tokenize.WordPunctTokenizer()
        tokens = tokenizer.tokenize(text.lower())
        tokens = [token for token in tokens if token not in self.stopwords]
        return ' '.join(tokens)

    def _get_embeddings(self, texts):
        inputs = self.tokenizer(texts, padding=True, truncation=True, return_tensors='pt')
        with torch.no_grad():
            outputs = self.model(**inputs)
        embeddings = torch.mean(outputs.last_hidden_state, dim=1)
        return embeddings

    def fit(self, samples):
        self.samples = samples
        processed_samples = [self._preprocess_text(sample) for sample in samples]
        self.samples_embeddings = self._get_embeddings(processed_samples)

    def _get_similarity(self, query):
        processed_query = self._preprocess_text(query)
        query_embedding = self._get_embeddings([processed_query])
        similarities = torch.cosine_similarity(query_embedding, self.samples_embeddings)
        return similarities

    def recommend(self, query, count = 10, mapped = True):
        similarity_scores = self._get_similarity(query)
        sorted_indices = similarity_scores.argsort(descending=True)
        print(sorted_indices)
        return [self.samples[index.item()] for index in sorted_indices]

if __name__ == '__main__':
    # Пример использования
    query= 'Я окончил только школу'
    samples = [
        'Высшее образование по специальности Информатика',
        'Магистратура по экономике',
        'Докторская степень в физике',
        'Среднее образование'
    ]
    model = NLPRecommender()
    model.fit(samples)
    recommendations = model.recommend(query)
    print(recommendations)

