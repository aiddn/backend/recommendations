import jwt
from fastapi import FastAPI, Depends
from fastapi.security import OAuth2PasswordBearer
from jwt import InvalidSignatureError, ExpiredSignatureError, DecodeError, ImmatureSignatureError
import asyncpg
from pydantic import BaseModel
import xxhash
from core import NLPRecommender
from exceptions import InvalidCredentials

app = FastAPI(title="Recommendation system API")

JWT_KEY = "SHMZXWy8jBUM}?B3'EJfwnt:}Uc\V=GgdaDWjkr4@u)f9muFD"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
model = NLPRecommender()


# pg_pool = asyncpg.create_pool(dsn="postgres://postgres:postgres@localhost:5401/profiles")


async def get_current_user(token: str = Depends(oauth2_scheme)):
    try:
        data = jwt.decode(token, JWT_KEY, algorithms=["HS256"])
    except (InvalidSignatureError, ExpiredSignatureError, DecodeError, ImmatureSignatureError) as e:
        raise InvalidCredentials
    return data


async def get_user(token: str = Depends(oauth2_scheme)):
    userdata: dict = await get_current_user(token)
    return userdata.get('sub')


class Data(BaseModel):
    profile: dict
    work_experience: dict


async def get_user_application(user_email: str):
    conn: asyncpg.Connection = await asyncpg.connect("postgres://postgres:postgres@localhost:5402/profiles")
    data = await conn.fetch(
        '''SELECT description, duration, place, position from work_experience where email = $1''', user_email)
    s = []
    [s.extend(list(i.values())) for i in data]
    return s


async def get_vacancies():
    conn: asyncpg.Connection = await asyncpg.connect("postgres://postgres:J3bslV1ekpaxyNW@localhost:5454/strapi")
    data = await conn.fetch(
        '''SELECT * from projects limit 200''')
    return data


@app.get(
    '/vacancies/recommendations'
)
async def get_recommendations_by_vacancies(
        user: dict = Depends(get_current_user)
):
    application = await get_user_application(user['sub']['email'])
    application_string = ' '.join(application)
    vacs = await get_vacancies()
    vacs_keys = {}
    v = []
    for i in vacs:
        s = i["name"] + " " + i["organizer"] + " " + i["short_text"] + " " + i["full_text"]
        key = xxhash.xxh64(s).hexdigest()
        vacs_keys[key] = i['id']
        v.append(s.replace("\n", ""))
    print(1)
    model.fit(v)
    recommendations = model.recommend(application_string)
    print(recommendations)
    return recommendations

    # if user['role'] != 'intern':
    #     raise InvalidCredentials
    # pass


@app.get(
    '/candidates/recommendation'
)
async def get_recommendations_by_vacancies(
        user: dict = Depends(get_current_user)
):
    pass

# if __name__ == '__main__':
#     uvicorn.run(app, host="0.0.0.0", port=9142)
