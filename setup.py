import nltk
import uvicorn
from transformers import AutoTokenizer, AutoModel
import json
import os

from api import app


def setup():
    # Загрузка стоп-слов, если файл отсутствует

    stopwords_file = './models/stopwords.json'
    if not os.path.isfile(stopwords_file):
        nltk.download('stopwords')
        stopwords = nltk.corpus.stopwords.words('russian')
        with open(stopwords_file, 'w') as file:
            json.dump(stopwords, file)
    
    # Загрузка модели, если файлы отсутствуют
    model_folder = './models/rubert-base-cased'
    tokenizer_file = os.path.join(model_folder, 'tokenizer')
    model_file = os.path.join(model_folder, 'model')
    if not os.path.isdir(model_folder):
        model_name = 'DeepPavlov/rubert-base-cased'
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        model = AutoModel.from_pretrained(model_name)
        tokenizer.save_pretrained(tokenizer_file)
        model.save_pretrained(model_file)

def print_directory(path):
    files = os.listdir(path)
    for file in files:
        print(file)

if __name__ == '__main__':
    print_directory('./models')
    setup()
    uvicorn.run(app, host="0.0.0.0", port=9142)

