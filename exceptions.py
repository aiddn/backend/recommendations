from typing import Optional

from starlette import status
from starlette.exceptions import HTTPException


class BaseAPIException(HTTPException):
    message: Optional[str] = "Base API Exception"
    status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR

    def __init__(self, message: Optional[str] = None):
        if message is not None:
            self.message = message

        super().__init__(status_code=self.status_code, detail=self.message)


class InvalidCredentials(BaseAPIException):
    message = "Could not validate credentials."
    status_code = status.HTTP_401_UNAUTHORIZED
